import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

import Player from '@/classes/Player'
import Dealer from '@/classes/Dealer'
import Deck from '@/classes/Deck'

export default new Vuex.Store({
    state: {
        player: new Player('Player 1'),
        dealer: new Dealer(),
        stock: []
    },
    mutations: {
        // add an X amount of decks to the stock
        fillStock(state, noDecks = 1, shuffle = true) {
            this.commit('clearStock');

            let deck;
            for (let index = 0; index < noDecks; index++) {
                deck = new Deck(shuffle)
                state.stock.push(...deck.cards)
            }
        },
        clearStock(state) {
            state.stock = [];
        }
    },
    actions: {

    }
})