import Vue from 'vue'
import App from './App.vue'

import Vuex from 'vuex'
Vue.use(Vuex)

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
Vue.use(Buefy)

// require('@/assets/main.scss')

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
