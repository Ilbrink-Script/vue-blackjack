// import BlackJack from './BlackJack';
import Status from './Status';

class BlackJack {
    static LIMIT = 21;
}

export default class Player {
    constructor(name, role = 'player') {
        this._name = name;
        this._role = role;
        this._hand = [];
    }

    // getters and setters
    get name() { return this._name; }
    get role() { return this._role; }
    get hand() { return this._hand; }
    set status(status) { this._status = status; }

    // calculates score of the hand
    get score() {
        let pts = 0;
        let aces = [];

        // take the sum of all non-ace cards
        this.hand.forEach(card => {
            if (card.faceUp) {
                if (card.name === 'ace') {
                    aces.push(card);
                } else {
                    pts += card.points;
                }
            }
        });

        // variable points for aces
        aces.forEach((ace, i, aces) => {
            // last ace can be either 1 or 11 points
            // if (Object.is(aces.length - 1, i)) {
            if (aces.length - 1 === i) {
                // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
                const min = pts + Math.min(...ace.points); // 1 pt
                const max = pts + Math.max(...ace.points); // 11 pts

                // use max score, as long as it's not over the game limit
                pts = max <= BlackJack.LIMIT ? max : min;
            }
            // there will be more aces, so always use 1 point (two 11 point aces = 22 = bust)
            else {
                pts += ace.points[0];
            }
        });

        return pts;
    }

    // returns the player status
    get status() {
        // fixed status
        if (this._status) {
            return this._status;
        }

        // score based status
        if (this.score > BlackJack.LIMIT) {
            return Status.BUST;
        } else if (this.score === BlackJack.LIMIT) {
            return Status.BLACKJACK;
        }
        // WAITING (dealer)
        else if (this.isDealer()) {
            return Status.WAITING
        }
        // CONTINUE (player)
        return Status.CONTINUE;
    }

    // is this the dealer? NO
    isDealer() {
        return false;
    }

    // receive a card from the deck
    dealCard(card) {
        this._hand.push(card);
    }

    // hit action
    hit() {
        this.dealCard(this._bj.getCard());
        this._bj.checkGameStatus(this);
    }

    // stand action
    stand() {
        this.status = Status.STAND;
        this._bj.checkGameStatus(this);
    }

    // resets the player status and returns the hand the player was holding
    reset() {
        // unset fixed status
        delete this._status;

        // empty hand
        return this._hand.splice(0);
    }
}