import Card from './Card'
export default class Deck {
    constructor(shuffle = false) {
        this._cards = [];

        this.fillCards();
        if (shuffle) {
            this.shuffle();
        }
    }

    // getters
    get cards() { return this._cards; }

    // add all cards to the deck
    fillCards() {
        Card.SUITS.forEach(suit => {
            for (let cardNum = 1; cardNum <= Card.CARDS_PER_SUIT; cardNum++) {
                this._cards.push(new Card(suit, cardNum));
            }
        });
    }

    // shuffle the cards in the deck
    shuffle() {
        const shuffledCards = [];
        let randomCard;
        while (this._cards.length > 0) {
            randomCard = this._cards.splice(Math.floor(Math.random() * this._cards.length), 1)[0];
            shuffledCards.push(randomCard);
        }
        this._cards = shuffledCards;
    }
}