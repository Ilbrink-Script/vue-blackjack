// object with the various player and game status
export default class Status {

    // player status
    static get CONTINUE() { return 'continue'; }
    static get WAITING() { return 'waiting'; }
    static get STAND() { return 'stand'; }
    static get BLACKJACK() { return 'blackjack! :)'; }
    static get BUST() { return 'bust :('; }
    static get WINNER() { return 'winner :)'; }
    static get LOSER() { return 'lost :('; }
    static get TIE() { return 'tied :|'; }

    // game status
    static get GAME_START() { return `Let's deal some hands.`; }
    static get GAME_PLAYER() { return `It's the player's turn.`; }
    static get GAME_DEALER() { return `It'the dealer's turn.`; }
    static get GAME_END() { return `The game has finished.`; }
}