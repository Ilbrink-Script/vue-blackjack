import Player from '@/classes/Player';
import Status from '@/classes/Status';

// console.log(typeof Player, new Player('Paultjen'));

export default class Dealer extends Player {
    constructor(name = 'Dealer') {
        super(name, 'dealer');
    }

    // is this the dealer? YES

    // turn over all cards
    showCards() {
        this.hand.forEach(card => card.show());
    }

    // do numerous checks to determine if dealer should hit
    shouldHit(plr) {
        // player bust
        if (plr.status === Status.BUST) return false

        // outscoring player
        if (this.score > plr.score) return false

        // 17 or higher
        if (this.score >= Dealer.STAND_AT_OR_ABOVE) return false

        // both at 17 points
        if (this.score === plr.score && this.score >= Dealer.STAND_AT_OR_ABOVE) return false;

        return true;
    }

    // dealer plays
    play() {
        // show cards
        this.showCards();

        // start hitting
        while (this.shouldHit()) {
            this.hit();
        }

        // stand if dealer could continue in theory
        if (this.status === Status.WAITING) { // was continue
            this.stand();
        }
        // dealer has hit blackjack or is bust
        else {
            this._bj.checkGameStatus(this);
        }
    }

    // the the value at which the dealer stands
    static get STAND_AT_OR_ABOVE() { return 17; }
}