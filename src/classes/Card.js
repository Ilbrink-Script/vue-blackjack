// card class
export default class Card {
    constructor(suit, number) {
        this._suit = suit;
        this._number = number;
        this._faceUp = false;
    }

    // getters
    get suit() { return this._suit; }
    get number() { return this._number; }
    get name() { return Card.NUMBER_TO_NAME(this.number); }
    get faceUp() { return this._faceUp; }

    // calculate point, ace can have 1 or 11 points
    get points() {
        if (this.number > 10) {
            return 10;
        } else if (this.number === 1) {
            return [1, 11];
        }
        return this.number;
    }

    // shows the card, turns it over
    show() { this._faceUp = true; }

    // shows the back of the card
    hide() { this._faceUp = false; }

    // a clean string representation of the card
    toString() { return `${this.name} of ${this.suit}`; }


    // static card properties
    static get SUITS() {
        return ['spades', 'hearts', 'clubs', 'diamonds'];
    }
    static get CARDS_PER_SUIT() {
        return 13;
    }

    // convertes the numeric value of a card to a name for the picture cards
    static NUMBER_TO_NAME(cardNum) {
        switch (cardNum) {
            case 1: return 'ace';
            case 11: return 'jack';
            case 12: return 'queen';
            case 13: return 'king';
            default: return cardNum;
        }
    }
}